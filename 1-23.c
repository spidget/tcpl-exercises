/*
 * Exercise 1-23. Write a program to remove all comments from a C program. Don't
 * forget to handle quoted strings and character constants properly. C comments
 * do not nest.
 */
#include <stdio.h>

#define CMTNOPE 0
#define CMTSTRT 1
#define CMTEND  2

#define INCMT  0
#define OUTCMT 1

#define INLIT  0
#define OUTLIT 1

int push(char *buf, char c)
{
        buf[0] = buf[1];
        buf[1] = c;

        return c;
}

int type(char *buf)
{
        if (*buf == '/' && *(buf+1) == '*')
                return CMTSTRT;
        if (*buf == '*' && *(buf+1) == '/')
                return CMTEND;
        return CMTNOPE;
}

int state(int cur, int type)
{
        switch (type) {
        case CMTSTRT:
                return INCMT;
        case CMTEND:
                return OUTCMT;
        default:
                return cur;
        }
}

int main(void)
{
        int s = OUTCMT;
        int l = OUTLIT;
        char buf[2] = {0};

        while (push(buf, getchar()) != EOF) {
                int t = type(buf);

                s = state(s, t);

                if (s == OUTCMT && (buf[1] == '"' || buf[1] == '\''))
                        l = !l;

                if (l == OUTLIT && (t == CMTSTRT || t == CMTEND))
                        buf[0] = buf[1] = '\0';

                if (l == INLIT || (s == OUTCMT && buf[0]))
                        putchar(buf[0]);
        }

        return 0;
}

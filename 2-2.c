/*
 * Exercise 2-2. Write a loop equivalent to the for loop above without using &&
 * or ¦¦.
 */
#include <stdio.h>

int main(void)
{
        int i;
        int c;
        int lim = 1000;
        char s[lim];

        for (i = 0; i<lim-1; i++)
                if ((c = getchar()) == '\n')
                        break;
                else if (c == EOF)
                        break;
                else
                        s[i] = c;
        s[i] = '\0';

        printf("%s\n", s);

        return 0;
}

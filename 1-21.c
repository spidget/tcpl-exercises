/*
 * Exercise 1-21. Write a program entab that replaces strings of blanks by the
 * minimum number of tabs and blanks to achieve the same spacing. Use the same
 * tab stops as for detab. When either a tab or a single blank would suffice to
 * reach a tab stop, which should be given preference?
 */
#include <ctype.h>
#include <stdio.h>

#define LIM     1000
#define TABSTOP 4

int getln(char *s, int lim)
{
        int c;
        int i = 0;

        while (i < lim-2 && (c = getchar()) != EOF && c != '\n')
                s[i++] = c;

        s[i] = '\0';

        return i;
}

void entab(char *s, int len)
{
        int col;
        int nspaces = 0;

        for (col = 0; col < len; col++) {
                if (isspace(s[col])) {
                        ++nspaces;
                        if ((col + 1) % TABSTOP == 0) {
                                putchar('\t');
                                nspaces = 0;
                        }
                } else {
                        while (nspaces > 0) {
                                putchar(' ');
                                --nspaces;
                        }
                        putchar(s[col]);
                }
        }

        putchar('\n');
}

int main(void)
{
        char s[LIM];
        int len;

        while ((len = getln(s, LIM)) > 0)
                entab(s, len);

        return 0;
}

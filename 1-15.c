/*
 * Exercise 1-15. Rewrite the temperature conversion program of Section 1.2 to
 * use a function for conversion.
 */
#include <stdio.h>

int cel(int fahr)
{
    return 5 * (fahr-32) / 9;
}

int main()
{
        int fahr;

        for (fahr = 0; fahr <= 200; fahr += 20)
                printf("%d\t%d\n", fahr, cel(fahr));

        return 0;
}

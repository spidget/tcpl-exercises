/*
 * Exercise 1-22. Write a program to "fold" long input lines into two or more
 * shorter lines after the last non-blank character that occurs before the n-th
 * column of input. Make sure your program does something intelligent with very
 * long lines, and if there are no blanks or tabs before the specified column.
 */
#include <ctype.h>
#include <stdio.h>

#define LIM       1000
#define LINEBREAK 80

int getln(char *s, int lim)
{
        int c;
        int i = 0;

        while (i < lim - 2 && (c = getchar()) != EOF && c != '\n')
                s[i++] = c;

        if (c == '\n')
                s[i++] = c;
        s[i] = '\0';

        return i;
}

int lastnb(char *s, int len)
{
        int i = len - 1;

        while (i-- && !isspace(s[i]))
                ;

        return ++i;
}

void fold(char *s, int len)
{
        int foldat, i;

        if (len < LINEBREAK) {
                printf("%s", s);
                return;
        }

        foldat = lastnb(s, LINEBREAK);
        foldat = foldat > 1 ? foldat : LINEBREAK;

        for (i = 0; i < foldat; i++)
                putchar(s[i]);
        putchar('\n');

        fold(&s[foldat], len - foldat);
}

int main(void)
{
        char s[LIM];
        int len;

        while ((len = getln(s, LIM)))
                fold(s, len);

        return 0;
}

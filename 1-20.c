/*
 * Exercise 1-20. Write a program detab that replaces tabs in the input with the
 * proper number of blanks to space to the next tab stop. Assume a fixed set of
 * tab stops, say every n columns. Should n be a variable or a symbolic
 * parameter?
 */
#include <stdio.h>

#define TABSTOP 8

int main(void)
{
        int i, c, col;

        col = 0;
        while ((c = getchar()) != EOF) {
                col = c == '\n' ? 0 : col + 1;

                if (c == '\t')
                        for (i = TABSTOP - col % TABSTOP; i > 0; i--)
                                putchar(' ');
                else
                        putchar(c);
        }

        return 0;
}

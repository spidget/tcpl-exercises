#include <stdio.h>

/* print Celsius-Fahrenheit table
    for cel = -100, -80, ..., 300; floating-point version */
int main()
{
        float fahr, celsius;
        int lower, upper, step;

        lower = -100;      /* lower limit of temperature table */
        upper = 300;    /* upper limit */
        step = 20;      /* step size */

        printf("cel     fahr\n");
        printf("------------\n");
        celsius = lower;
        while (celsius <= upper) {
                fahr = (1.8 * celsius) + 32;
                printf("%4.0f %7.1f\n", celsius, fahr);
                celsius = celsius + step;
        }
}

/*
 * Exercise 1-17. Write a program to print all input lines that are longer than
 * 80 characters.
 */
#include <stdio.h>

#define LIM 3

void flush(char *buf, int num)
{
        int i = 0;

        while (i < num)
                putchar(*(buf + i++));
}

int main(void)
{
        int i, c;
        char buf[LIM + 1];

        i = 0;
        while ((c = getchar()) != EOF) {
                if (c == '\n') {
                        if (i >= LIM)
                                putchar(c);
                        i = 0;
                } else if (i < LIM) {
                        buf[i++] = c;
                } else if (i == LIM) {
                        buf[i++] = '\0';
                        printf("%s%c", buf, c);
                } else {
                        putchar(c);
                }
        }

        return 0;
}

/*
 * Exercise 1-19. Write a function reverse(s) that reverses the
 * character string s. Use it to write a program that reverses its input a line
 * at a time.
 */
#include <stdio.h>

#define LIM 1000

int getln(char *s, int lim);
void reverse(char *s, int len);

int main(void)
{
        char s[LIM];
        int len;

        while ((len = getln(s, LIM))) {
                reverse(s, len - 1);
                printf("%s\n", s);
        }

        return 0;
}

int getln(char *s, int lim)
{
        int i, c;

        for (i = 0; i < lim && (c = getchar()) != EOF && c != '\n'; i++)
                s[i] = c;

        return i;
}

void swap(char *one, char *two)
{
        char tmp;

        tmp = *one;
        *one = *two;
        *two = tmp;
}

void reverse(char *s, int len)
{
        int i;

        for (i = 0; i < len; i++, len--)
                swap(s + i, s + len);
}

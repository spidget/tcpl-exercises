/*
 * Exercise 1-12. Write a program that prints its input one word per line.
 */
#include <stdio.h>

#define WORD  0
#define SPACE 1

int main(void)
{
        int c, state;

        state = WORD;
        while ((c = getchar()) != EOF)
                if (c == ' ') {
                        if (state == WORD)
                                printf("\n");
                        state = SPACE;
                } else {
                        printf("%c", c);
                        state = WORD;
                }

        return 0;
}

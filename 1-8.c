#include <stdio.h>

int main(void)
{
        int c, nb, nt, nl;

        nb = nt = nl = 0;
        while ((c = getchar()) != EOF)
                switch (c) {
                case ' ':
                        ++nb;
                        break;
                case '\t':
                        ++nt;
                        break;
                case '\n':
                        ++nl;
                        break;
                }
        printf("blanks: %d, tabs: %d, newlines: %d\n", nb, nt, nl);

        return 0;
}

/*
 * Exercise 1-18. Write a program to remove trailing blanks and tabs from each
 * line of input, and to delete entirely blank lines.
 */
#include <stdio.h>

#define LIM 80

int getln(char s[], int lim)
{
        int c, i = 0;
        
        while (i < lim-1 && (c = getchar()) != EOF && c != '\n')
                s[i++] = c;

        if (c == '\n')
                s[i++] = c;

        s[i] = '\0';

        return i;
}

void rstrip(char *s, int len)
{
        len--;
        while (s[len] == ' ' || s[len] == '\t' || s[len] == '\n')
                s[len--] = '\0';

}

int main(void)
{
        char len, line[LIM];

        while ((len = getln(line, LIM))) {
                rstrip(line, len);
                printf("<%s>\n", line);
        }

        return 0;
}

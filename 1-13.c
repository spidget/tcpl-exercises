/*
 * Exercise 1-13. Write a program to print a histogram of the lengths of words
 * in its input. It is easy to draw the histogram with the bars horizontal; a
 * vertical orientation is more challenging.
 */
#include <stdio.h>

#define WORD  0
#define SPACE 1

int main(void)
{
        int c, wl, state;

        state = WORD;
        wl = 0;
        while ((c = getchar()) != EOF)
                if (c == ' ' || c == '\t' || c == '\n') {
                        if (state == WORD) {
                                while (--wl > 0)
                                        putchar('-');
                                printf("\n");
                        }
                        state = SPACE;
                } else {
                        state = WORD;
                        ++wl;
                }

        return 0;
}

/*
 * Exercise 1-24. Write a program to check a C program for rudimentary syntax
 * errors like unbalanced parentheses, brackets and braces. Don’t forget about
 * quotes, both single and double, escape sequences, and comments. (This program
 * is hard if you do it in full generality.)
 */
#include <stdio.h>

#define CODE    0
#define CHAR    1
#define STRING  2
#define COMMENT 3

#define TSTUFF   0
#define TCMTSTRT 1
#define TCMTEND  2
#define TCHAR    3
#define TSTRING  4

struct counts {
        int parens;
        int brackets;
        int braces;
};

int push(char *buf, int c)
{
        *buf = *(buf+1);
        return *(buf+1) = c;
}

int type(char *buf)
{
        if (*buf == '/' && *(buf+1) == '*')
                return TCMTSTRT;
        if (*buf == '*' && *(buf+1) == '/')
                return TCMTEND;
        if (*buf != '\\')
                switch (*(buf+1)) {
                case '"':
                        return TSTRING;
                case '\'':
                        return TCHAR;
                }
        return TSTUFF;
}

int state(int cur, char *buf)
{
        int t = type(buf);

        switch (cur) {
        case CHAR:
                return t == TCHAR ? CODE : CHAR;
        case STRING:
                return t == TSTRING ? CODE : STRING;
        case COMMENT:
                return t == TCMTEND ? CODE : COMMENT;
        }

        switch (t) {
        case TCHAR:
                return CHAR;
        case TSTRING:
                return STRING;
        case TCMTSTRT:
                return COMMENT;
        }

        return CODE;
}

void count(struct counts *counts, int c)
{
        switch (c) {
        case '(':
                counts->parens++;
                break;
        case ')':
                counts->parens--;
                break;
        case '[':
                counts->brackets++;
                break;
        case ']':
                counts->brackets--;
                break;
        case '{':
                counts->braces++;
                break;
        case '}':
                counts->braces--;
                break;
        }
}

int main(void)
{
        int s = CODE;
        char buf[2] = {0, 0};
        struct counts counts = {0, 0, 0};

        while (push(buf, getchar()) != EOF)
                if ((s = state(s, buf)) == CODE)
                        count(&counts, buf[1]);

        printf("parens: %d, brackets: %d, braces: %d\n",
               counts.parens,
               counts.brackets,
               counts.braces);

        return 0;
}

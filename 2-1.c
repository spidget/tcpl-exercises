/*
 * Write a program to determine the ranges of char, short, int, and long
 * variables, both signed and unsigned, by printing appropriate values from
 * standard headers and by direct computation. Harder if you compute them:
 * determine the ranges of the various floating-point types.
 */
#include <float.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>

#define LDBL_BYTES 10

void schar(void)
{
        signed char sc = 1 << (sizeof(signed char) * 8 - 1);

        printf("signed char min:\t%d\n", sc);
        printf("signed char min:\t%d\n", CHAR_MIN);
        printf("signed char max:\t%d\n", ~sc);
        printf("signed char max:\t%d\n", CHAR_MAX);
        printf("signed char max:\t%d\n", SCHAR_MAX);
}

void uchar(void)
{
        unsigned char uc = 0;
        printf("unsigned char min:\t%u\n", uc);

        uc = ~0;
        printf("unsigned char max:\t%u\n", uc);
        printf("unsigned char max:\t%u\n", UCHAR_MAX);
}

void sshort(void)
{
        signed short ss = 1 << (sizeof(signed short) * 8 - 1);

        printf("signed short min:\t%d\n", ss);
        printf("signed short min:\t%d\n", SHRT_MIN);
        printf("signed short max:\t%d\n", ~ss);
        printf("signed short max:\t%d\n", SHRT_MAX);
}

void ushort(void)
{
        unsigned short us = 0;

        printf("unsigned short min:\t%u\n", us);

        us = ~0;
        printf("unsigned short max:\t%u\n", us);
        printf("unsigned short max:\t%u\n", USHRT_MAX);
}

void sint(void)
{
        signed int si = 1 << (sizeof(signed int) * 8 - 1);

        printf("signed int min:\t\t%d\n", si);
        printf("signed int min:\t\t%d\n", INT_MIN);
        printf("signed int max:\t\t%d\n", ~si);
        printf("signed int max:\t\t%d\n", INT_MAX);
}

void uint(void)
{
        unsigned int ui = 0;

        printf("unsigned int min:\t%u\n", ui);

        ui = ~0;
        printf("unsigned int max:\t%u\n", ui);
        printf("unsigned int max:\t%u\n", UINT_MAX);
}

void slong(void)
{
        signed long sl = 1l << (sizeof(signed long) * 8 - 1);

        printf("signed long min:\t%ld\n", sl);
        printf("signed long min:\t%ld\n", LONG_MIN);
        printf("signed long max:\t%ld\n", ~sl);
        printf("signed long max:\t%ld\n", LONG_MAX);
}

void ulong(void)
{
        unsigned long ul = 0;

        printf("unsigned long min:\t%lu\n", ul);

        ul = ~0;
        printf("unsigned long max:\t%lu\n", ul);
        printf("unsigned long max:\t%lu\n", ULONG_MAX);
}

/*
 * I can't mangle the bits in a float, but I can use a union to type pun them
 * out.
 */
void floa(void)
{
        union {
                float val;
                struct {
                        unsigned int sig: 23;
                        unsigned int exp: 8;
                        unsigned int sign: 1;
                };
        } f;

        f.val = 0;

        f.sign = 1;
        f.exp = 0xfe;
        f.sig = 0x7fffff;

        printf("float min:\t\t%g\n", f.val);
        printf("float min:\t\t%g\n", -FLT_MAX);

        f.sign = 0;
        printf("float max:\t\t%g\n", f.val);
        printf("float max:\t\t%g\n", FLT_MAX);
}

/*
 * double is larger than an integer on this machine. There is no guarantee that
 * a bitfield will not have a gap if it crosses a word boundary. I can memcpy
 * the bits into a array of unsigned chars though.
 */
void doubl(void)
{
        int i;
        double d = 0;
        unsigned char b[sizeof d];

        for (i = 0; i < sizeof d; i++)
                b[i] = 0xff;
        b[sizeof d - 2] &= ~0x10; /* set the tailing bit in the exponent to 0
                                     to stop it being a nan value. */
        memcpy(&d, b, sizeof d);

        printf("double min:\t\t%g\n", d);
        printf("double min:\t\t%g\n", -DBL_MAX);

        b[sizeof d - 1] &= ~0x80; /* unset the sign bit */
        memcpy(&d, b, sizeof d);

        printf("double max:\t\t%g\n", d);
        printf("double max:\t\t%g\n", DBL_MAX);
}

void ldoubl(void)
{
        int i;
        long double ld;
        unsigned char b[10]; /* gcc implements long double using the 80bit
                                extended precision format. I can't use sizeof
                                here because it reports the size as 128bits */

        for (i = 0; i < 10; i++)
                b[i] = 0xff;
        b[8] &= ~0x01; /* all 1s in exponent means nan, trailing 0 is the
                          largest possible value. */
        memcpy(&ld, b, sizeof ld);

        printf("long double min:\t%Lg\n", ld);
        printf("long double min:\t%Lg\n", -LDBL_MAX);

        b[9] &= ~0x80; /* unset sign bit */
        memcpy(&ld, b, sizeof ld);

        printf("long double max:\t%Lg\n", ld);
        printf("long double max:\t%Lg\n", LDBL_MAX);
}

int main(void)
{
        schar();
        uchar();
        sshort();
        ushort();
        sint();
        uint();
        slong();
        ulong();
        floa();
        doubl();
        ldoubl();

        return 0;
}

/*
 * Exercise 1-13. Write a program to print a histogram of the lengths of words
 * in its input. It is easy to draw the histogram with the bars horizontal; a
 * vertical orientation is more challenging.
 */
#include <stdio.h>

#define WORD        0
#define SPACE       1
#define MAXWORDS    100

int main(void)
{
        int c, state, wc, wl, longest, i;
        int words[MAXWORDS];

        wc = wl = longest = 0;
        for (i = 0; i < MAXWORDS; i++)
                words[i] = 0;

        state = WORD;

        while ((c = getchar()) != EOF && wc < MAXWORDS)
                if (c == ' ' || c == '\t' || c == '\n') {
                        if (state == WORD) {
                                words[wc++] = wl;
                                wl = 0;
                        }
                        state = SPACE;
                } else {
                        ++wl;
                        if (wl > longest)
                                longest = wl;
                        state = WORD;
                }

        for (c = longest; c > 0; c--) {
                for (i = 0; i < wc; i++)
                        if (words[i] < c)
                                printf("-");
                        else
                                printf("|");

                printf("\n");
        }

        return 0;
}

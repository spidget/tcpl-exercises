/*
 * Exercise 1-14. Write a program to print a histogram of the frequencies of
 * different characters in its input.
 */
#include <stdio.h>

#define MAXCHARS 256

int main(void)
{
        int c, i;
        int freq[MAXCHARS];

        for (i = 0; i < MAXCHARS; i++)
                freq[i] = 0;

        while ((c = getchar()) != EOF)
                freq[c]++;

        for (i = 0; i < MAXCHARS; i++)
                if (freq[i]) {
                        if (i > 31 && i < 127)
                                printf("%c\t|", i);
                        else
                                printf("0x%X\t|", i);

                        for (c = 0; c < freq[i]; c++)
                                putchar('-');

                        putchar('\n');
                }

        return 0;
}

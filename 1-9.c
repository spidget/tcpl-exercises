#include <stdio.h>

int main(void)
{
        int c, space;

        space = 0;

        while ((c = getchar()) != EOF) {
                if (c == ' ') {
                        if (space == 0)
                                printf("%c", c);
                        space = 1;
                } else {
                        space = 0;
                        printf("%c", c);
                }
        }
        printf("\n");

        return 0;
}
